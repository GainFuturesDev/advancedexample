using System;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Accounts;
using GF.Api.Allocation;
using GF.Api.Utils;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Program's main form
    /// </summary>
    public partial class MainForm : Form
    {
        private readonly LoginForm _loginForm;
        private readonly IGFClient _gfClient;
        private readonly System.Windows.Forms.Timer _timer;

        /// <summary>
        ///     Construct main form and preparing common events
        /// </summary>
        public MainForm()
        {
            _gfClient = GF.Api.Impl.GFApi.CreateClient();

            _timer = new Timer { Interval = (int)TimeSpan.FromMilliseconds(10).TotalMilliseconds };
            _timer.Tick += (sender, args) => OnTimerTick();
            _timer.Start();

            _loginForm = new LoginForm(_gfClient);
            InitializeComponent();

            _loginForm.OnConnectionStatusChange += OnConnectionStatusChange;
            ordersControl.OnCurrentOrderChanged += orderDetailsControl1.CurrentOrderChangedEventHandler;
            chartsControl1.OnChartSelectedContractChanged += histogramControl1.ChartSelectedContractChangedEventHandler;

            _gfClient.Logging.ErrorOccurred += GfClientOnError;
            selectAccountToolStripMenuItem.Enabled = false;
        }

        private void OnTimerTick()
        {
            _gfClient.Threading.Advance();
        }

        /// <summary>
        ///     Displays error occured
        /// </summary>
        /// <param name="client"></param>
        /// <param name="args"></param>
        private void GfClientOnError(IGFClient client, ErrorEventArgs args)
        {
            MessageBox.Show(args.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        ///     Occurs when connection status is changed
        /// </summary>
        /// <param name="newConnectionStatus">New connection status</param>
        private void OnConnectionStatusChange(ConnectionStatus newConnectionStatus)
        {
            lblConnectionStatus.Text = LoginForm.GetConnectionStatusInfo(newConnectionStatus);
            Update();

            selectAccountToolStripMenuItem.Enabled = _gfClient.Connection.Aggregate.IsConnected;

            if (_gfClient.Connection.Aggregate.IsConnected)
            {
                LoadAccounts();
                CheckCurrentAccount();
                CheckAllocationBlockWarnings();
            }
        }

        private void CheckAllocationBlockWarnings()
        {
            var warnings = _gfClient.Allocation.Get()
                .Where(item => _gfClient.Allocation.Validate(item).Any( ))
                .SelectMany(item => _gfClient.Allocation.Validate(item))
                .Select(error => error.Message).ToList();

            if (warnings.Count > 0)
                MessageBox.Show("Allocation blocks warnings:\n" + string.Join(Environment.NewLine, warnings.ToArray()));
        }

        /// <summary>
        ///     Loads accounts and allocation blocks into Account menu item's dropdown list
        /// </summary>
        private void LoadAccounts()
        {
            selectAccountToolStripMenuItem.DropDownItems.Clear();

            //load accounts
            foreach (var account in _gfClient.Accounts.Get())
                AddAccountMenuItem(account);

            if (!_gfClient.Accounts.Wash.Get().Any())
                return;

            allocationBlocksToolStripMenuItem.Visible = true;

            if (!_gfClient.Allocation.Get().Any())
                return;

            selectAccountToolStripMenuItem.DropDownItems.Add(new ToolStripSeparator());

            foreach (var ab in _gfClient.Allocation.Get())
                AddAccountMenuItem(ab);
        }

        /// <summary>
        ///     Mark current account or allocation block in Accounts menu item sub items
        /// </summary>
        private void CheckCurrentAccount()
        {
            Globals.CurrentAccountOrAB = new AccountOrAB(_gfClient.Accounts.Get().First());
            if (Globals.CurrentAccountOrAB.Account != null)
            {
                ((ToolStripMenuItem) selectAccountToolStripMenuItem.DropDownItems[Globals.CurrentAccountOrAB.ToString()]
                    )
                    .Checked = true;
                UpdateCurrentAccount();
            }
        }

        /// <summary>
        ///     Adds account or AB into menu item's drop down list
        /// </summary>
        /// <param name="item"></param>
        private void AddAccountMenuItem(object item)
        {
            var menuItem = new ToolStripMenuItem(item.ToString()) {Tag = item, Name = item.ToString()};
            menuItem.Click += accountMenuItem_Click;

            selectAccountToolStripMenuItem.DropDownItems.Add(menuItem);
        }

        /// <summary>
        ///     Changes current selected account or AB when user clicks it in menu item's drop down list
        /// </summary>
        private void accountMenuItem_Click(object sender, EventArgs e)
        {
            var clickedMenuItem = (ToolStripMenuItem) sender;

            if (clickedMenuItem.Checked)
                return;

            foreach (ToolStripItem menuItem in selectAccountToolStripMenuItem.DropDownItems)
            {
                if (menuItem is ToolStripMenuItem toolStripMenuItem)
                    toolStripMenuItem.Checked = false;
            }

            clickedMenuItem.Checked = true;

            switch (clickedMenuItem.Tag)
            {
                case IAccount account:
                    Globals.CurrentAccountOrAB = new AccountOrAB(account);
                    break;
                case IAllocationBlock allocationBlock:
                    Globals.CurrentAccountOrAB = new AccountOrAB(allocationBlock);
                    break;
            }

            UpdateCurrentAccount();
        }

        /// <summary>
        ///     Updates current account info in status bar
        /// </summary>
        private void UpdateCurrentAccount()
        {
            lblAccountInfo.Text = "Current account: " + Globals.CurrentAccountOrAB;
        }

        /// <summary>
        ///     Showing Login Window on MainForm shown
        /// </summary>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            _loginForm.ShowDialog();
        }

        /// <summary>
        ///     Handling login menu item click
        /// </summary>
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _loginForm.ShowDialog();
        }

        /// <summary>
        ///     Handling Exit menu item click
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        ///     Shows allocation blocks edit form
        /// </summary>
        private void allocationBlocksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new AllocationBlocksForm(_gfClient);
            if (f.ShowDialog() == DialogResult.OK)
            {
                LoadAccounts();
                CheckCurrentAccount();
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            _gfClient.Connection.Aggregate.Disconnect();
        }

        private void toolStripMenuItemShowLog_Click(object sender, EventArgs e)
        {
            LogForm.ShowLog(_gfClient);
        }
    }
}