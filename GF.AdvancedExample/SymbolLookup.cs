﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Contracts;
using GF.Api.Contracts.Criteria;
using GF.Api.Contracts.Groups;
using GF.Api.Contracts.Lookup;
using GF.Api.Contracts.Lookup.Request;
using GF.Api.Values.Contracts;

namespace GF.AdvancedExample
{

    /// <summary>
    /// Represents API symbol lookup functionality
    /// </summary>
    public partial class SymbolLookup : BaseControl
    {
        private SymbolLookupRequest _criteria;
        private SymbolLookupRequestID _criteriaID;


        /// <summary>
        /// Construct symbol lookup control
        /// </summary>
        public SymbolLookup(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Prepare symbol lookup handler (for search results) and login complete handler (for filters initializing)
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            Client.Contracts.Lookup.SymbolLookupReceived += GlobalSymbolLookupReceived;
            Client.Connection.Aggregate.LoginCompleted += (o,e) => InitializeFilters();
            if (Client.Connection.Aggregate.IsConnected)
                InitializeFilters();
        }

        /// <summary>
        /// Initializing Exchange and ContractGroup filters, used for fitering symbol lookup results
        /// </summary>
        private void InitializeFilters()
        {
            cbExchangeFilter.Items.Clear();
            cbExchangeFilter.Items.Add("All");
            foreach (var ex in Client.Exchanges.Get())
                cbExchangeFilter.Items.Add(ex.Name);
            cbExchangeFilter.SelectedIndex = 0;

            cbContractGroup.Items.Clear();
            cbContractGroup.Items.Add("All");
            foreach (var group in Client.Contracts.Groups.Get())
                cbContractGroup.Items.Add(group);
            cbContractGroup.SelectedIndex = 0;
        }

        /// <summary>
        /// Fullfills results table once results are available
        /// </summary>
        /// <param name="symbolLookup"></param>
        /// <param name="contracts"></param>
        void GlobalSymbolLookupReceived(IGFClient client, SymbolLookupEventArgs e)
        {
            if (_criteria == null || _criteriaID != e.RequestID)
                return;

            lvResults.Items.Clear();
            var contractList = new List<IContract>(e.Contracts);
            contractList.Sort(new LookupContractSorter(_criteria.Symbol.Text));
            foreach (var contract in contractList)
            {
                var item = new ListViewItem(contract.Symbol);
                item.SubItems.Add(contract.Description);
                item.SubItems.Add(contract.BaseContract.ContractKind.ToString());
                item.SubItems.Add(contract.Exchange.ToString());
                item.SubItems.Add(contract.ContractGroup.ToString());
                item.SubItems.Add(contract.StartTime.ToString());
                item.SubItems.Add(contract.StopTime.ToString());
                lvResults.Items.Add(item);
            }
        }

        /// <summary>
        /// Performs contracts lookup
        /// </summary>
        private void SearchClick(object sender, EventArgs e)
        {
            string searchText = edText.Text.Trim();
            if (string.IsNullOrEmpty(searchText))
            {
                MessageBox.Show("Search text should not be empty");
                return;
            }
            if (!Client.Connection.Aggregate.IsConnected)
            {
                MessageBox.Show("Not connected");
                return;
            }

            var builder = new SymbolLookupRequestBuilder()
                .WithResultCount((int) edMaxResults.Value)
                .WithSymbol(edText.Text, GF.Api.Values.Contracts.Lookup.TextSearchMode.StartsWith)
                .WithExpression(new ContractKindsCriterion(GetSelectedContractKinds().ToList().AsReadOnly()));

            if (Client.Exchanges.Get(cbExchangeFilter.Text) != null)
                builder = builder.WithExpression(
                    new ExchangeIDCriterion(Client.Exchanges.Get(cbExchangeFilter.Text).ID));

            if (cbContractGroup.SelectedItem is ContractGroup @group)
                builder = builder.WithExpression(new ContractGroupIDCriterion(@group.ID));

            _criteria = builder.Build();

            try
            {
                _criteriaID = Client.Contracts.Lookup.ByCriteria(_criteria);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured during lookup" + ex);
            }

        }

        /// <summary>
        /// Gathers values from contract kind checkboxes (Forex, Equity, Options)
        /// </summary>
        private IEnumerable<ContractKind> GetSelectedContractKinds()
        {
            if (chbFutures.Checked)
            {
                yield return ContractKind.Continuous;
                yield return ContractKind.Future;
                yield return ContractKind.FutureCompound;
                yield return ContractKind.GenericCompound;
            }
        }
    }

    class LookupContractSorter : IComparer<IContract>
    {
        private readonly string _searchText;

        public LookupContractSorter(string searchText)
        {
            _searchText = searchText;
        }

        public int Compare(IContract x, IContract y)
        {
            int res = GetSymbolLookupPriority(x, _searchText).CompareTo(GetSymbolLookupPriority(y, _searchText));
            if (res == 0)
                res = x.Symbol.CompareTo(y.Symbol);
            return res;
        }

        private static int GetSymbolLookupPriority(IContract c, string searchText)
        {
            string symbol = c.Symbol.ToUpper();
            if (symbol == searchText)
                return 0;
            if (symbol.StartsWith(searchText))
                return 1;
            string descr = c.Description.ToUpper();
            if (descr.StartsWith(searchText))
                return 2;
            if (symbol.Contains(searchText))
                return 3;
            if (descr.Contains(searchText))
                return 3;
            return 100;
        }
    }
}
