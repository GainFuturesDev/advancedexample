using GF.Api;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     This is a parent control for controls that allow user to subscribe contract
    /// </summary>
    public partial class SubscribeControl : BaseControl
    {
        /// <summary>
        ///     Subscribe control constructor
        /// </summary>
        public SubscribeControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }
    }
}