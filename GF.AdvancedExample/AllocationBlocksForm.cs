﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Accounts;
using GF.Api.Allocation;
using GF.Api.Values.AllocBlock;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Allocation blocks editing form. Loads, saves and edits allocation blocks
    /// </summary>
    public partial class AllocationBlocksForm : Form
    {
        private readonly IGFClient _gfClient;
        /// <summary>
        ///     Allocation blocks form constructor
        /// </summary>
        public AllocationBlocksForm(IGFClient gfClient)
        {
            _gfClient = gfClient;
            InitializeComponent();
            PrepareRulesCombo();
            PrepareAccountsCombo();
        }

        /// <summary>
        ///     Gets available accounts list from column's in-place editor combo box
        /// </summary>
        private IEnumerable<IAccount> AvailableAccounts
            => (IEnumerable<IAccount>) accountIdDataGridViewComboBoxColumn.DataSource;

        /// <summary>
        ///     Gets current allocation block item
        /// </summary>
        private BlockInProgress SelectedBlock
            => cbAB.SelectedItem as BlockInProgress;

        /// <summary>
        ///     Loads account list to column's in-place editor combo box
        /// </summary>
        private void PrepareAccountsCombo()
        {
            var accounts = new List<IAccount>(_gfClient.Accounts.Get());
            accountIdDataGridViewComboBoxColumn.DataSource = accounts;
            accountIdDataGridViewComboBoxColumn.ValueMember = "ID";
            accountIdDataGridViewComboBoxColumn.DisplayMember = "Spec";
        }

        /// <summary>
        ///     Fills Allocation Rules
        /// </summary>
        private void PrepareRulesCombo()
        {
            cbRule.DataSource = Enum.GetValues(typeof (AllocationRule));
            cbRule.SelectedItem = null;
        }

        /// <summary>
        ///     Loads existing allocaction blocks
        /// </summary>
        private void AllocationBlocksForm_Load(object sender, EventArgs e)
        {
            SetABListCombo(_gfClient.Allocation.Get().Select(t=> new BlockInProgress(t)));
            UpdateDeleteAllocationBlockButtonStatus();
        }

        /// <summary>
        ///     Fills combo with loaded allocation blocks
        /// </summary>
        /// <param name="abList">Allocation block list</param>
        private void SetABListCombo(IEnumerable<BlockInProgress> abList)
        {
            cbAB.Items.Clear();

            foreach (var ab in abList)
                cbAB.Items.Add(ab);

            if (cbAB.Items.Count > 0)
                cbAB.SelectedIndex = 0;
        }

        /// <summary>
        ///     Disables delete button, if no more items to delete
        /// </summary>
        private void UpdateDeleteAllocationBlockButtonStatus()
        {
            btDeleteAB.Enabled = cbAB.Items.Count > 0;
        }

        /// <summary>
        ///     Validates and saves AllocationBlocks if OK button pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AllocationBlocksForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                return;

            string errMsg;
            if (!ValidateAllocationBlocks(out errMsg))
            {
                MessageBox.Show(errMsg);
                e.Cancel = true;
                return;
            }

            try
            {
                _gfClient.Allocation.SubmitBlocks(GetABListCombo().Select(b=>b.ToTemplate()).ToList());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error saving allocation blocks.\n" + ex.Message);
                e.Cancel = true;
                return;
            }

            e.Cancel = false;
        }

        /// <summary>
        ///     Validating allocation blocks being edited
        /// </summary>
        /// <param name="errMsg">Error message when validation is false</param>
        /// <returns>True if validation ok, otherwise false</returns>
        private bool ValidateAllocationBlocks(out string errMsg)
        {
            foreach (BlockInProgress ab in cbAB.Items)
            {
                if (ab.Items.Count < 2)
                {
                    errMsg = ab + " has less than 2 accounts";
                    return false;
                }
                foreach (AllocationBlockItemTemplate abItem in ab.Items)
                {
                    if (abItem.Lots < 1)
                    {
                        errMsg = ab + " - " + abItem + " not valid";
                        return false;
                    }
                }
            }
            errMsg = string.Empty;
            return true;
        }

        /// <summary>
        ///     Adding allocation block to list
        /// </summary>
        private void btAddAB_Click(object sender, EventArgs e)
        {
            if (cbAB.Text.Trim() == string.Empty || cbRule.SelectedItem == null)
                return;

            string name = cbAB.Text;

            if (cbAB.FindStringExact(name) > -1)
                return;

            var rule = (AllocationRule) cbRule.SelectedItem;

            try
            {
                var ab = new AllocationBlockTemplate(name, rule, new List<AllocationBlockItemTemplate>());

                cbAB.Items.Add(ab);
                cbAB.SelectedItem = ab;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error adding allocation blocks.\n" + ex.Message);
                return;
            }

            UpdateDeleteAllocationBlockButtonStatus();
        }

        /// <summary>
        ///     Reads allocation block list from combo
        /// </summary>
        /// <returns>Allocation block list</returns>
        private IReadOnlyList<BlockInProgress> GetABListCombo()
            => cbAB.Items.Cast<BlockInProgress>().ToList();


        /// <summary>
        ///     Rebinds items list for new selected allocation block
        /// </summary>
        private void cbAB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedBlock == null)
                return;
            ReBindABItemList(SelectedBlock);
            cbRule.SelectedItem = SelectedBlock.Rule;
        }

        /// <summary>
        ///     Apply new selected rule
        /// </summary>
        private void cbRule_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedBlock == null)
                return;
            SelectedBlock.SetRule((AllocationRule) cbRule.SelectedItem);
        }

        /// <summary>
        ///     Deleting Allocation block
        /// </summary>
        private void btDeleteAB_Click(object sender, EventArgs e)
        {
            cbAB.Items.Remove(SelectedBlock);

            if (cbAB.Items.Count > 0)
            {
                cbAB.SelectedIndex = 0;
            }
            else
            {
                cbAB.Text = string.Empty;
            }

            ReBindABItemList(SelectedBlock);

            UpdateDeleteAllocationBlockButtonStatus();
        }

        /// <summary>
        ///     Bind items of new allocation block to item list grid
        /// </summary>
        /// <param name="newAB">New Allocation Block</param>
        private void ReBindABItemList(BlockInProgress newAB)
        {
            var bs = (BindingSource) dgABItems.DataSource;

            if (newAB != null)
            {
                bs.DataSource = null;
                bs.DataSource = newAB.InprogressItems;
            }
            else
            {
                bs.DataSource = null;
            }

            bs.ResetBindings(false);
        }

        /// <summary>
        ///     Adds new item to selected allocation block item list
        /// </summary>
        private void btAddItem_Click(object sender, EventArgs e)
        {
            if (SelectedBlock == null)
                return;

            var account = GetNextAvailableAccount();

            if (account == null)
                return;

            SelectedBlock.AddItem(account.ID, 1);

            ReBindABItemList(SelectedBlock);
            UpdateDeleteAllocationBlockButtonStatus();
        }

        /// <summary>
        ///     Gets next account that is not in list of already selected accounts
        /// </summary>
        /// <returns></returns>
        private IAccount GetNextAvailableAccount()
        {
            foreach (var account in AvailableAccounts)
            {
                bool isExists = false;
                foreach (var abItem in SelectedBlock.Items)
                {
                    if (abItem.AccountID == account.ID)
                    {
                        isExists = true;
                        break;
                    }
                }
                if (!isExists)
                    return account;
            }
            return null;
        }


        /// <summary>
        ///     Deletes selected item from item list of current selected allocation block
        /// </summary>
        private void btDeleteItem_Click(object sender, EventArgs e)
        {
            if (SelectedBlock == null)
                return;
            var bs = (BindingSource) dgABItems.DataSource;
            if (bs.Current != null)
                SelectedBlock.RemoveItem((BlockInProgress.BlockInProgressItem) bs.Current);
            ReBindABItemList(SelectedBlock);
        }

        /// <summary>
        ///     Avoid exception throw of datagrid, when in-place editor's combo has no match account (maybe deleted)
        /// </summary>
        private void dgABItems_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }

        /// <summary>
        ///     Validates lots column's cells for integer values
        /// </summary>
        private void dgABItems_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == lotDataGridViewTextBoxColumn.Index)
            {
                string value = e.FormattedValue.ToString();
                int lot;
                if (int.TryParse(value, out lot))
                    return;
                MessageBox.Show("Value must by of integer type");
                dgABItems[e.ColumnIndex, e.RowIndex].Value = 1;
            }
        }

        private class BlockInProgress
        {
            private string _name;
            private AllocationRule _rule;
            private List<BlockInProgressItem> _items = new List<BlockInProgressItem>();

            public BlockInProgress(string name, AllocationRule rule)
            {
                SetName(name);
                SetRule(rule);
            }

            public BlockInProgress(AllocationBlockTemplate allocationBlock)
            {
                SetName(allocationBlock.Name);
                SetRule(allocationBlock.Rule);
                foreach (var i in allocationBlock.Items)
                    AddItem(i.AccountID, i.Lots);
            }

            public string Name => _name;
            public AllocationRule Rule => _rule;
            public IReadOnlyList<AllocationBlockItemTemplate> Items => _items.Select(i=> new AllocationBlockItemTemplate(i.AccountID, i.Lots)).ToList();
            public IList<BlockInProgressItem> InprogressItems => _items;

            public void SetName(string name)
                => _name = name;

            public void SetRule(AllocationRule rule)
                => _rule = rule;

            public void AddItem(AccountID accountID, int lots)
                => _items.Add(new BlockInProgressItem { AccountID = accountID, Lots = lots });

            public void RemoveItem(BlockInProgressItem item)
                => _items.Remove(item);

            public override string ToString()
                => Name;

            public AllocationBlockTemplate ToTemplate()
                => new AllocationBlockTemplate(_name, _rule, Items);

            public class BlockInProgressItem
            {
                public AccountID AccountID { get; set; }

                public int Lots { get; set; }
            }
        }

    }
}