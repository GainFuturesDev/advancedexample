using System;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Orders;
using GF.Api.Orders.Drafts;
using GF.Api.Orders.ExtendedData;
using GF.Api.Values.Orders;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Provides visual controls for fulfilling Order.
    /// </summary>
    public partial class OrderDraftControl : BaseControl
    {
        /// <summary>
        ///     Creates Order draft control
        /// </summary>
        public OrderDraftControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Preparing login complete event handler
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Connection.Aggregate.LoginCompleted += OnLoginComplete;
        }


        /// <summary>
        ///     Makes some preparetions when login completes successfully
        /// </summary>
        private void OnLoginComplete(IGFClient client, GF.Api.Connection.LoginCompleteEventArgs e)
        {
            PrepareFlags();
            PrepareOrderTypes();
        }

        /// <summary>
        ///     Reads order side from radiobox
        /// </summary>
        /// <returns></returns>
        private OrderSide GetOrderSide()
        {
            if (rbBuy.Checked)
                return OrderSide.Buy;
            if (rbSell.Checked)
                return OrderSide.Sell;
            return OrderSide.None;
        }


        /// <summary>
        ///     Prepares Order types selection combo
        /// </summary>
        private void PrepareOrderTypes()
        {
            cbOrderType.Items.AddRange(new object[]
            {
                OrderType.Market,
                OrderType.Limit,
                OrderType.Stop,
                OrderType.StopLimit,
                OrderType.MarketOnOpen,
                OrderType.MarketOnClose,
                OrderType.MarketOnPitOpen,
                OrderType.MarketOnPitClose,
                OrderType.Iceberg
            });
        }

        /// <summary>
        ///     Prepares flag selection combo
        /// </summary>
        private void PrepareFlags()
        {
            cbFlags.Items.AddRange(new object[]
            {
                OrderFlags.None,
                OrderFlags.GTC,
                OrderFlags.FOK,
                OrderFlags.IOC
            });

            cbFlags.SelectedIndex = 0;
        }

        protected bool CheckForValidSettings(out int qty, out int volume, out double? price, out double? price2)
        {
            qty = 0;
            volume = 0;
            price = null;
            price2 = null;

            if (cbSymbol.SelectedContract == null)
                return false;

            if (!int.TryParse(cbQty.Text, out qty))
                return false;

            if (!rbBuy.Checked && !rbSell.Checked)
                return false;

            if (cbOrderType.SelectedItem == null)
                return false;

            if (cbFlags.SelectedItem == null)
                return false;

            if (cbVolume.Text.Trim() != string.Empty)
                if (!int.TryParse(cbVolume.Text, out volume))
                    return false;

            if (edPrice.Text.Trim() != string.Empty)
            {
                if (!double.TryParse(edPrice.Text, out var price_))
                    return false;
                price = price_;
            }
            if (edPrice2.Text.Trim() != string.Empty)
            {
                if (!double.TryParse(edPrice2.Text, out var price2_))
                    return false;
                price2 = price2_;
            }
            return true;
        }

        /// <summary>
        ///     Gets order draft data from UI controls
        /// </summary>
        /// <param name="draft">Existing order draft for modifying. If null, new order will be created. </param>
        /// <param name="comments">Order comments</param>
        /// <returns>Returns created or modified order draft. Returns null if impossible to read draft from UI</returns>
        protected bool FillOutOrderDraft(ModifyOrderDraftBuilder draft)
        {
            if (!CheckForValidSettings(out var qty, out var volume, out var price, out var price2))
                return false;

            draft.WithQuantity(qty);

            draft.WithTrailingStop(null);
            draft.WithExecutionInstruction(ExecInst.None);
            draft.WithOrderType((OrderType) cbOrderType.SelectedItem);

            draft.WithIceberg((OrderType)cbOrderType.SelectedItem == OrderType.Iceberg
                ? new IcebergData(volume)
                : null);

            draft.WithStart(cbTimeStart.Checked
                ? cbTimeStart.Value.ToUniversalTime()
                : (DateTime?)null);
            draft.WithEnd(cbTimeEnd.Checked
                ? cbTimeEnd.Value.ToUniversalTime()
                : (DateTime?)null);
            draft.WithPrice(price);
            draft.WithPrice2(price2);
            var modifyErrors = Client.Orders.Drafts.Validate(draft.Build());
            if (modifyErrors.Any())
            {
                MessageBox.Show($"Check : {string.Join(", ", modifyErrors.Select(e => e.Message))}");
                return false;
            }

            return true;
        }


        protected bool FillOutOrderDraft(OrderDraftBuilder draft, string comments)
        {
            if (!CheckForValidSettings(out var qty, out var volume, out var price, out var price2))
                return false;

            Globals.SetDraftAccount(Client, draft);

            draft.WithContractID(cbSymbol.SelectedContract.ID);
            draft.WithFlags((OrderFlags) cbFlags.SelectedItem);
            draft.WithSide(GetOrderSide());
            draft.WithComments(comments);
            if (string.IsNullOrEmpty(comments))
                return false;

            draft.WithQuantity(qty);
            draft.WithTrailingStop(null);
            draft.WithExecutionInstruction(ExecInst.None);
            draft.WithOrderType((OrderType) cbOrderType.SelectedItem);
            draft.WithIceberg(
                (OrderType) cbOrderType.SelectedItem == OrderType.Iceberg ? new IcebergData(volume) : null);

            draft.WithStart(cbTimeStart.Checked ? cbTimeStart.Value.ToUniversalTime() : (DateTime?) null);
            draft.WithEnd(cbTimeEnd.Checked ? cbTimeEnd.Value.ToUniversalTime() : (DateTime?) null);
            draft.WithPrice(price);
            draft.WithPrice2(price2);

            var errors = Client.Orders.Drafts.Validate(draft.Build());
            if (errors.Any())
            {
                MessageBox.Show($"Check : {string.Join(", ", errors.Select(e => e.Message))}");
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Sets UI controls values from order item
        /// </summary>
        /// <param name="order">Order to take data from</param>
        protected void SetDraftFromOrder(IOrder order)
        {
            rbBuy.Checked = (order.Side == OrderSide.Buy);
            rbSell.Checked = (order.Side == OrderSide.Sell);
            cbQty.Text = order.Quantity.ToString();
            cbSymbol.SelectedContract = order.Contract;
            cbSymbol.Refresh();

            cbOrderType.SelectedItem = order.Type;
            cbFlags.SelectedItem = order.Flags;
            edPrice.Text = order.Price.ToString();
            edPrice2.Text = order.Price2.ToString();

            if (order.Versions.Current.Start.HasValue)
            {
                var localTime = order.Versions.Current.Start.Value.ToLocalTime();
                if (localTime > cbTimeStart.MinDate && localTime < cbTimeStart.MaxDate)
                {
                    cbTimeStart.Checked = true;
                    cbTimeStart.Value = localTime;
                }
            }

            if (order.Versions.Current.End.HasValue)
            {
                var localTime = order.Versions.Current.End.Value.ToLocalTime();
                if (localTime > cbTimeEnd.MinDate && localTime < cbTimeEnd.MaxDate)
                {
                    cbTimeEnd.Checked = true;
                    cbTimeEnd.Value = localTime;
                }
            }
        }

        /// <summary>
        ///     Clears controls
        /// </summary>
        protected void ClearOrderDraft()
        {
            rbBuy.Checked = false;
            rbSell.Checked = false;
            cbSymbol.SelectedContract = null;
            cbQty.Text = string.Empty;

            cbFlags.SelectedItem = null;
            cbOrderType.SelectedItem = 0;
            cbVolume.Text = string.Empty;
            edPrice.Text = string.Empty;
            edPrice2.Text = string.Empty;
            cbTimeEnd.Checked = cbTimeStart.Checked = false;
        }

        /// <summary>
        ///     Changes Price selection area, when Order type is reselected
        /// </summary>
        private void cbOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            edPrice.Visible = edPrice2.Visible = cbVolume.Visible =
                lbPrice.Visible = lbPrice2.Visible = lbVolume.Visible = false;

            lbPrice.Text = "Stop";

            switch ((OrderType) cbOrderType.SelectedItem)
            {
                case OrderType.Limit:
                    lbPrice.Visible = edPrice.Visible = true;
                    lbPrice.Text = "Limit";
                    break;
                case OrderType.Stop:
                    lbPrice.Visible = edPrice.Visible = true;
                    lbPrice.Visible = edPrice.Visible = true;
                    lbPrice.Text = "Stop";
                    break;
                case OrderType.StopLimit:
                    lbPrice.Visible = lbPrice2.Visible = edPrice.Visible = edPrice2.Visible = true;
                    lbPrice.Text = "Stop";
                    lbPrice2.Text = "Limit";
                    break;
                case OrderType.Iceberg:
                    lbPrice.Visible = edPrice.Visible = lbVolume.Visible = cbVolume.Visible = true;
                    lbPrice.Text = "Limit";
                    break;
            }
        }

        /// <summary>
        ///     Sets current price to price control if subscribed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbSymbol_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbSymbol.SelectedContract == null)
                return;

            edPrice.Text = string.Empty;

            if (cbSymbol.SelectedContract.CurrentPrice != null)
                edPrice.Text = cbSymbol.SelectedContract.CurrentPrice.LastPrice.ToString();
        }
    }
}