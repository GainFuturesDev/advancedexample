using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Accounts;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Balance control. Visualizes account summary
    /// </summary>
    public partial class BalanceControl : BaseControl
    {
        /// <summary>
        ///     Balance control constructor
        /// </summary>
        public BalanceControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
            NullAllIndicators();
        }

        /// <summary>
        ///     Prepares balance event handlers
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            Client.Accounts.AccountSummaryChanged += Client_OnAccountSummaryChanged;
            Client.Accounts.BalanceChanged += Client_OnBalanceChanged;

            Globals.OnCurrentAccountChanged += Globals_OnCurrentAccountChanged;
            UpdateBalance(Globals.CurrentAccountOrAB);
        }

        /// <summary>
        ///     Occurs when current account changed
        /// </summary>
        private void Globals_OnCurrentAccountChanged(AccountOrAB accountOrBlock)
        {
            UpdateBalance(accountOrBlock);
        }

        /// <summary>
        ///     Refresh balance control when account summary is changed
        /// </summary>
        private void Client_OnAccountSummaryChanged(IGFClient client, AccountSummaryChangedEventArgs e)
        {
            UpdateBalance(new AccountOrAB(e.Account));
        }

        /// <summary>
        ///     Called when balance required to be updated
        /// </summary>
        private void Client_OnBalanceChanged(IGFClient client, BalanceChangedEventArgs e)
        {
            UpdateBalance(new AccountOrAB(e.Account));
        }

        /// <summary>
        ///     Update Balance Control
        /// </summary>
        private void UpdateBalance(AccountOrAB accountOrBlock)
        {
            if (accountOrBlock == null)
                return;

            if (Globals.CurrentAccountOrAB == accountOrBlock || Globals.CurrentAccountOrAB.HasAccount(accountOrBlock.Account))
            {
                var balance = accountOrBlock.TotalBalance;
                if (balance == null)
                {
                    NullAllIndicators();
                    return;
                }
                SetActiveText(lbOpenPL, balance.OpenPnL);
                SetActiveText(lbRealizedPL, balance.RealizedPnL);
                SetActiveText(lbTotalPL, balance.OpenPnL + balance.RealizedPnL);

                SetActiveText(lbNetLiq, balance.NetLiquidatingValue - balance.SettlePnL + balance.OpenPnL);
                SetActiveText(lbCash, balance.NetLiquidatingValue - balance.SettlePnL);
            }
        }

        /// <summary>
        ///     Clears all indicators
        /// </summary>
        private void NullAllIndicators()
        {
            lbNetLiq.Text = lbOpenPL.Text = lbRealizedPL.Text = lbTotalPL.Text = string.Empty;
        }

        /// <summary>
        ///     Sets values to indicators with appropriate color (green when positive, red when negative, black when zero
        /// </summary>
        /// <param name="lb">Indicator (Label control) on which to make changes </param>
        /// <param name="value">Value which to set into indicator</param>
        private void SetActiveText(Label lb, double value)
        {
            lb.ForeColor = (value < 0) ? Color.Red : ((value > 0) ? Color.Green : Color.Black);

            var nfo = new NumberFormatInfo {CurrencySymbol = string.Empty};
            lb.Text = value.ToString("C", nfo);
        }
    }
}