using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Accounts;
using GF.Api.Connection;
using GF.Api.Orders.Drafts;
using GF.Api.Orders.Drafts.Validation;
using GF.Api.Positions;
using GF.Api.Values.Orders;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Average positions control. Visualize current average positions of account, provides exit position functionality
    /// </summary>
    public partial class AveragePositionsControl : BaseControl
    {
        /// <summary>
        ///     Position list binding source. Used to store current selected position
        /// </summary>
        private readonly BindingSource _positionsBindingSource = new BindingSource();

        /// <summary>
        ///     Average positions control constructor
        /// </summary>
        public AveragePositionsControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Prepares average positions related event handlers
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Accounts.AvgPositionChanged += Accounts_OnAvgPositionChanged;
            Client.Connection.Aggregate.LoginCompleted += Aggregate_OnLoginComplete;
            Globals.OnCurrentAccountChanged += Globals_OnCurrentAccountChanged;
            UpdatePositions(null);
        }

        private void Aggregate_OnLoginComplete(IGFClient client, LoginCompleteEventArgs e)
        {
            UpdatePositions(null);
        }

        private void Accounts_OnAvgPositionChanged(IGFClient client, PositionChangedEventArgs e)
        {
            UpdatePositions(e.Account);
        }

        /// <summary>
        ///     Occurs when current account changed
        /// </summary>
        void Globals_OnCurrentAccountChanged(AccountOrAB accountOrBlock)
        {
            UpdatePositions(null);
        }

        /// <summary>
        ///     Updates Average position control
        /// </summary>
        public void UpdatePositions(IAccount account)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            if (account == null || Globals.CurrentAccountOrAB.HasAccount(account))
            {

                _positionsBindingSource.DataSource = AvgPositionListEntry.ConvertFromOecAvgPositionList();

                dgAvgPositions.DataSource = _positionsBindingSource;
            }
        }


        /// <summary>
        ///     Attempts to close position
        /// </summary>
        private void btExit_Click(object sender, EventArgs e)
        {
            if (_positionsBindingSource.Current == null)
                return;

            var pos = (AvgPositionListEntry) _positionsBindingSource.Current;

            var orderDraft = new OrderDraftBuilder()
                .WithAccountID(pos.Position.Account.ID)
                .WithSide(pos.Position.Net.Volume < 0 ? OrderSide.Buy : OrderSide.Sell)
                .WithContractID(pos.Position.PositionContract.ID)
                .WithQuantity(Math.Abs(pos.Position.Net.Volume))
                .WithOrderType(OrderType.Market)
                .Build();

            if (orderDraft.Quantity == 0)
                return; // nothing to close

            IReadOnlyList<OrderDraftValidationError> validationErrors = Client.Orders.Drafts.Validate(orderDraft);
            if (validationErrors.Any())
            {
                MessageBox.Show(string.Join(Environment.NewLine, validationErrors.Select(error => error.Message)),
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (MessageBox.Show($"Send {orderDraft} ?", "Confirmation", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) ==
                    DialogResult.No)
                    return;

                Client.Orders.SendOrder(orderDraft);
            }
        }

        /// <summary>
        ///     Highlight position which can be closed
        /// </summary>
        private void dgAvgPositions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var element = (AvgPositionListEntry) _positionsBindingSource[e.RowIndex];
            e.CellStyle.ForeColor = element.NetPos == 0 ? Color.Gray : Color.Black;
        }
    }

    /// <summary>
    ///     Used for simple representation(visualization) of average positions
    /// </summary>
    internal class AvgPositionListEntry
    {
        /// <summary>
        ///     Native position
        /// </summary>
        private readonly IPosition _position;

        /// <summary>
        ///     Creates instance
        /// </summary>
        /// <param name="position">Native OEC API position on which to create instance</param>
        private AvgPositionListEntry(IPosition position)
        {
            _position = position;
        }

        /// <summary>
        ///     Position contract
        /// </summary>
        public string Contract => _position.Contract.ToString();

        /// <summary>
        ///     Position bought number
        /// </summary>
        public int BoughtNum => _position.Long.Volume;

        /// <summary>
        ///     Position bought average price
        /// </summary>
        public string BoughtAvgPrice => _position.Contract.PriceToString(_position.Long.Price);

        /// <summary>
        ///     Position sold number
        /// </summary>
        public int SoldNum => _position.Short.Volume;

        /// <summary>
        ///     Position sold average price
        /// </summary>
        public string SoldAvgPrice => _position.Contract.PriceToString(_position.Short.Price);

        /// <summary>
        ///     Net position
        /// </summary>
        public int NetPos => _position.Net.Volume;

        /// <summary>
        ///     Total profit/loss
        /// </summary>
        public string TotalPnL => _position.Gain.ToString("#,0.00");

        /// <summary>
        ///     Open profit/loss
        /// </summary>
        public string OpenPnL => _position.OTE.ToString("#,0.00");

        /// <summary>
        ///     Realized profit/loss
        /// </summary>
        public string RealizedPnL => _position.NetGain.ToString("#,0.00");

        /// <summary>
        ///     Native position instance
        /// </summary>
        public IPosition Position => _position;

        /// <summary>
        ///     Gets converted positions of account
        /// </summary>
        /// <returns></returns>
        public static IList<AvgPositionListEntry> ConvertFromOecAvgPositionList()
        {
            return
                ListConverter<IPosition, AvgPositionListEntry>.FromIEnumerable(
                    Globals.CurrentAccountOrAB.AvgPositions, AvgPositionListConverter);

        }

        /// <summary>
        ///     Converts native position to position list entry
        /// </summary>
        private static AvgPositionListEntry AvgPositionListConverter(IPosition position)
        {
            return new AvgPositionListEntry(position);
        }
    }
}