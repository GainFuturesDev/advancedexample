﻿using System;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Connection;

namespace GF.AdvancedExample
{
    /// <inheritdoc />
    /// <summary>
    ///     Login form
    /// </summary>
    public partial class LoginForm : Form
    {
        private readonly IGFClient _gfClient;
        /// <summary>
        ///     Login form constructor. Prepares login related events
        /// </summary>
        public LoginForm(IGFClient client)
        {
            _gfClient = client;
            InitializeComponent();

            _gfClient.Connection.Aggregate.LoginCompleted += OnLoginComplete;
            _gfClient.Connection.Aggregate.LoginFailed += OnLoginFailed;
            _gfClient.Connection.Aggregate.Disconnected += OnDisconnected;
        }

        /// <summary>
        ///     Event, that occurs when connection status is changed
        /// </summary>
        public event ConnectionStatusChangeEventHandler OnConnectionStatusChange;

        /// <summary>
        ///     Occurs when disconnected from client
        /// </summary>
        private void OnDisconnected(IGFClient client, DisconnectedEventArgs e)
        {
            if (e.Exception != null)
                MessageBox.Show("Unexpected disconnection occurs");

            OnConnectionStatusChange(ConnectionStatus.Disconnected);
        }

        /// <summary>
        ///     Occurs when login failed
        /// </summary>
        private void OnLoginFailed(IGFClient client, LoginFailedEventArgs e)
        {
            MessageBox.Show("Login Failed : " + e.FailReason);
            OnConnectionStatusChange(ConnectionStatus.Disconnected);
        }

        /// <summary>
        ///     Occurs when login successful
        /// </summary>
        private void OnLoginComplete(IGFClient client, LoginCompleteEventArgs e)
        {
            OnConnectionStatusChange(ConnectionStatus.Connected);
            //foreach (var bc in _gfClient.Contracts.Base.Get().Where(bc=> bc.IsFuture && bc.ContractGroup.Name == "Indices"))
            //    _gfClient.Contracts.Lookup.ByBaseContractID(bc.ID);
        }

        /// <summary>
        ///     User presses login button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btLogin_Click(object sender, EventArgs e)
        {
            try
            {
                _gfClient.Connection.Aggregate.Disconnect();
                OnConnectionStatusChange(ConnectionStatus.Connecting);
                _gfClient.Connection.Aggregate.Connect(
                    new ConnectionContextBuilder()
                        .WithUserName(edLogin.Text)
                        .WithPassword(edPass.Text)
                        .WithUUID("9e61a8bc-0a31-4542-ad85-33ebab0e4e86")
                        .WithPort(9210)
                        .WithHost("api.gainfutures.com")
                        .WithForceLogin(true)
                        .Build());
            }
            catch (Exception ex)
            {
                OnConnectionStatusChange(ConnectionStatus.Disconnected);
                MessageBox.Show("Error during connection : " + ex.Message);
                return;
            }

            Close();
        }

        /// <summary>
        ///     Gets connection status comment depends on connection status enumeration
        /// </summary>
        /// <param name="connectionStatus">Connection status to analyze</param>
        /// <returns>Return connection status comment</returns>
        public static string GetConnectionStatusInfo(ConnectionStatus connectionStatus)
        {
            switch (connectionStatus)
            {
                case ConnectionStatus.Connected:
                    return "Connected";
                case ConnectionStatus.Connecting:
                    return "Connecting...";
                case ConnectionStatus.Disconnected:
                    return "Disconnected";
                default:
                    throw new ArgumentException("Unknown connection status");
            }
        }
    }


    /// <summary>
    ///     Connection status changed event handler
    /// </summary>
    /// <param name="newConnectionStatus"></param>
    public delegate void ConnectionStatusChangeEventHandler(ConnectionStatus newConnectionStatus);


    /// <summary>
    ///     Connection status enumeration
    /// </summary>
    public enum ConnectionStatus
    {
        /// <summary>
        ///     Connected to server
        /// </summary>
        Connected,

        /// <summary>
        ///     Not connected
        /// </summary>
        Disconnected,

        /// <summary>
        ///     Trying to connect
        /// </summary>
        Connecting
    }
}