using System;
using System.Linq;
using GF.Api;
using GF.Api.Contracts;
using GF.Api.Subscriptions.Bars;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Current contract selection changed event handler
    /// </summary>
    /// <param name="contract">Newly selected contract</param>
    public delegate void ChartSelectedContractChangedEventHandler(IContract contract);

    /// <summary>
    ///     Charts control. Visualize contract OLHC price chart
    /// </summary>
    public partial class ChartsControl : BaseControl
    {
        /// <summary>
        ///     Chart control constructor
        /// </summary>
        public ChartsControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Occurs when current selected contract changed
        /// </summary>
        public event ChartSelectedContractChangedEventHandler OnChartSelectedContractChanged;

        /// <summary>
        ///     Prepares price chart control events
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Subscriptions.Bars.BarsReceived += OnBarsReceived;
            Client.Subscriptions.Histogram.HistogramReceived += Histogram_OnHistogramReceived;
        }

        private void Histogram_OnHistogramReceived(IGFClient client, GF.Api.Subscriptions.Histograms.HistogramReceivedEventArgs e)
        {
            Console.WriteLine("");
        }

        /// <summary>
        ///     Another contract selected
        /// </summary>
        private void cbContracts_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnChartSelectedContractChanged?.Invoke(cbSymbol.SelectedContract);
            RefreshChart();
        }

        /// <summary>
        ///     Another time scale selected
        /// </summary>
        private void cbTimeScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshChart();
        }

        /// <summary>
        ///     Refreshes chart
        /// </summary>
        private void RefreshChart()
        {
            chart.ClearChart();
            dgBars.DataSource = null;

            if (cbSymbol.SelectedContract == null)
                return;

            var interval = float.Parse((string) cbTimeScale.SelectedItem);

            Client.Subscriptions.Bars.Subscribe(
                cbSymbol.SelectedContract.ID,
                Client.Subscriptions.Bars.Duration.Create(
                    DateTime.UtcNow - TimeSpan.FromMinutes(interval * chart.IntervalCnt), DateTime.UtcNow),
                Client.Subscriptions.Bars.Description.CreateMinutes((int) interval)
            );
        }

        /// <summary>
        ///     Draws chart when Bars for selected contract received
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnBarsReceived(IGFClient client, BarsReceivedEventArgs e)
        {
            if (cbSymbol.SelectedContract == null)
                return;

            if (cbSymbol.SelectedContract.ID != e.Subscription.Contract.ID)
                return;

            if (e.Bars.Count == 0)
                return;

            chart.DrawChart(e.Subscription.Contract.Description, e.Bars.ToArray());
            BarEntry[] barTable = Array.ConvertAll(e.Bars.ToArray(), BarEntry.BarEntryConverter);
            dgBars.DataSource = barTable;
        }
    }

    /// <summary>
    ///     Represent chart bar for visualization
    /// </summary>
    public class BarEntry
    {
        private readonly Bar _bar;

        /// <summary>
        ///     Creates bar entry from OEC API Bar
        /// </summary>
        /// <param name="bar"></param>
        public BarEntry(Bar bar)
        {
            _bar = bar;
        }

        /// <summary>
        ///     Bar close value
        /// </summary>
        public double Close => _bar.Close;

        /// <summary>
        ///     Bar high value
        /// </summary>
        public double High => _bar.High;

        /// <summary>
        ///     Bar low value
        /// </summary>
        public double Low => _bar.Low;

        /// <summary>
        ///     Bar open value
        /// </summary>
        public double Open => _bar.Open;

        /// <summary>
        ///     Bar time stamp
        /// </summary>
        public DateTime TimeStamp => _bar.OpenTimestamp.ToLocalTime();

        /// <summary>
        ///     Converts OEC API bar to internal bar entry
        /// </summary>
        /// <param name="bar">OEC API Bar</param>
        /// <returns>Bar entry</returns>
        public static BarEntry BarEntryConverter(Bar bar)
        {
            return new BarEntry(bar);
        }
    }
}