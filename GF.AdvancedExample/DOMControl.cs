using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Contracts;
using GF.Api.Subscriptions.Doms;

namespace GF.AdvancedExample
{
    /// <inheritdoc />
    /// <summary>
    ///     Contract DOM control. Displays DOM of contract
    /// </summary>
    public partial class DomControl : SubscribeControl
    {
        private readonly Dictionary<ContractID, IDomSubscription> _subscribedContracts = new Dictionary<ContractID, IDomSubscription>(100);

        /// <inheritdoc />
        /// <summary>
        ///     DOM control constructor
        /// </summary>
        public DomControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }


        /// <inheritdoc />
        /// <summary>
        ///     Prepares DOM changed event
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Subscriptions.Dom.DomChanged += OnDomChanged;
        }

        /// <summary>
        ///     Occurs when DOM for some contract changed. if this contract is selected contract, refresh DOM Grid
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnDomChanged(IGFClient client, DomChangedEventArgs e)
        {
            var currentContract = cbSymbol.SelectedContract;

            if (currentContract == null || currentContract.ID != e.Contract.ID)
                return;

            RefreshDom(e.Contract);
        }

        /// <summary>
        ///     Another contract selected, DOM grid will be refreshed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbSymbol_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshDom(cbSymbol.SelectedContract);
        }

        /// <summary>
        ///     Refreshes DOM grid
        /// </summary>
        /// <param name="contract"></param>
        private void RefreshDom(IContract contract)
        {
            if (contract == null)
            {
                dgDOMAsk.DataSource = dgDOMBid.DataSource = null;
                return;
            }

            RefreshSubscribeButtons(contract);

            if (!IsDomAvailable(contract))
            {
                dgDOMAsk.DataSource = dgDOMBid.DataSource = null;
                return;
            }

            dgDOMAsk.DataSource = GetDOMAskEntryArrayFromDOM(contract.DOM);
            dgDOMBid.DataSource = GetDOMBidEntryArrayFromDOM(contract.DOM);
        }

        /// <summary>
        ///     Creates and fulfills array of Contract DOM Ask Entries, to simple represent it in DOM Ask Grid
        /// </summary>
        /// <param name="dom"></param>
        /// <returns></returns>
        private ContractDomAskEntry[] GetDOMAskEntryArrayFromDOM(IDom dom)
        {
            int length = dom.AskLevels.Count;
            var entries = new ContractDomAskEntry[length];

            for (int i = 0; i < length; i++)
                entries[i] = new ContractDomAskEntry(dom.AskLevels[i], dom.AskSizes[i]);

            return entries;
        }

        /// <summary>
        ///     Creates and fulfills array of Contract DOM Bid Entries, to simple represent it in DOM Bid Grid
        /// </summary>
        /// <param name="dom"></param>
        /// <returns></returns>
        private ContractDomBidEntry[] GetDOMBidEntryArrayFromDOM(IDom dom)
        {
            int length = dom.BidLevels.Count;
            var entries = new ContractDomBidEntry[length];

            for (int i = 0; i < length; i++)
                entries[i] = new ContractDomBidEntry(dom.BidLevels[i], dom.BidSizes[i]);

            return entries;
        }

        /// <summary>
        ///     Refreshes Subscribe and Unsubscribe buttons enable statuses
        /// </summary>
        /// <param name="contract"></param>
        private void RefreshSubscribeButtons(IContract contract)
        {
            btSubscribe.Enabled = !IsDomAvailable(contract);
            btUnsubscribe.Enabled = !btSubscribe.Enabled;
        }

        /// <summary>
        ///     Determine if DOM for contract is not null, and contains data
        /// </summary>
        /// <param name="contract"></param>
        /// <returns>True if DOM contains data, otherwise false</returns>
        private bool IsDomAvailable(IContract contract)
        {
            return _subscribedContracts.ContainsKey(contract.ID);
        }

        /// <summary>
        ///     Tries to subscribe contract for DOM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSubscribe_Click(object sender, EventArgs e)
        {
            var contract = cbSymbol.SelectedContract;
            if (contract == null)
                return;
            try
            {
                _subscribedContracts.Add(contract.ID, Client.Subscriptions.Dom.Subscribe(contract.ID));
                RefreshSubscribeButtons(contract);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't subscribe contract for DOM. " + ex.Message);
            }
        }

        /// <summary>
        ///     Tries to unsubscribe contract for DOM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btUnsubscribe_Click(object sender, EventArgs e)
        {
            var contract = cbSymbol.SelectedContract;
            if (contract == null)
                return;

            try
            {
                if (_subscribedContracts.TryGetValue(contract.ID, out var subscription))
                {
                    subscription.Unsubscribe();
                    _subscribedContracts.Remove(contract.ID);
                    dgDOMAsk.DataSource = dgDOMBid.DataSource = null;
                    RefreshSubscribeButtons(contract);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't unsubscribe contract for DOM. " + ex.Message);
            }
        }
    }

    /// <summary>
    ///     Used for simple representation of DOM data Bids
    /// </summary>
    public class ContractDomBidEntry
    {
        private readonly float _bidLevel;
        private readonly float _bidSize;

        /// <summary>
        ///     Create instance of DOM data Bids entry
        /// </summary>
        /// <param name="bidLevel">Bid level</param>
        /// <param name="bidSize">Bid size</param>
        public ContractDomBidEntry(float bidLevel, uint bidSize)
        {
            _bidLevel = bidLevel;
            _bidSize = bidSize;
        }

        /// <summary>
        ///     Bid level
        /// </summary>
        public float BidLevel => _bidLevel;

        /// <summary>
        ///     Bid size
        /// </summary>
        public float BidSize => _bidSize;
    }

    /// <summary>
    ///     Used for simple representation of DOM data Asks
    /// </summary>
    public class ContractDomAskEntry
    {
        private readonly float _askLevel;

        private readonly uint _askSize;

        /// <summary>
        ///     Create instance of DOM data Asks entry
        /// </summary>
        /// <param name="askLevel">Ask level</param>
        /// <param name="askSize">Ask size</param>
        public ContractDomAskEntry(float askLevel, uint askSize)
        {
            _askLevel = askLevel;
            _askSize = askSize;
        }

        /// <summary>
        ///     Ask level
        /// </summary>
        public float AskLevel => _askLevel;

        /// <summary>
        ///     Ask size
        /// </summary>
        public uint AskSize => _askSize;
    }
}