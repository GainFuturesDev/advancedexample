using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Contracts;
using GF.Api.Contracts.Criteria;
using GF.Api.Contracts.Lookup;
using GF.Api.Contracts.Lookup.Request;
using GF.Api.Values.Contracts;
using GF.Api.Values.Contracts.Lookup;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Contract ComboBox control, that shows current loaded contracts
    /// </summary>
    public class ContractCombo : ComboBox
    {
        private readonly IGFClient _client;

        public ContractCombo(GF.Api.IGFClient client)
        {
            _client = client;
        }

        /// <summary>
        ///     Selected Contract
        /// </summary>
        public IContract SelectedContract
        {
            get => SelectedItem as IContract;
            set => SelectedItem = value;
        }

        protected override void OnFormat(ListControlConvertEventArgs e)
        {
            e.Value = e.ListItem is IContract contract ? $"{contract.Symbol} {contract.Description}" : string.Empty;
        }

        protected override void OnDropDown(EventArgs e)
        {
            DataSource = _client.Contracts.Get();
            base.OnDropDown(e);
        }
    }


    /// <summary>
    ///     Contract lookup combo box, that uses lookup resulst in drop down list
    /// </summary>
    public class ContractLookupCombo : ComboBox
    {
        private SymbolLookupRequestID _requestID;
        private readonly IGFClient _client;

        public ContractLookupCombo(GF.Api.IGFClient client)
        {
            _client = client;
        }

        /// <summary>
        ///     Selected Contract
        /// </summary>
        public IContract SelectedContract
        {
            get => SelectedItem as IContract;
            set => SelectedItem = value;
        }

        /// <summary>
        ///     Contract kind used for results filtering
        /// </summary>
        public ContractKind ContractKind { get; set; }

        protected override void OnTextChanged(EventArgs e)
        {
            if (SelectedItem != null && Text == SelectedItem.ToString())
                return;

            if (string.IsNullOrEmpty(Text))
            {
                Items.Clear();
                try
                {
                    DroppedDown = false;
                }
                catch
                {
                }
                return;
            }

            _requestID = _client.Contracts.Lookup.ByCriteria(
                new SymbolLookupRequestBuilder()
                    .WithSymbol(Text, TextSearchMode.StartsWith)
                    .WithExpression(new ContractKindsCriterion(new[] {ContractKind}))
                    .Build());
        }

        protected override void OnCreateControl()
        {
            _client.Contracts.Lookup.SymbolLookupReceived += OnSymbolLookupReceived;
        }

        private void OnSymbolLookupReceived(IGFClient client, SymbolLookupEventArgs e)
        {
            if (_requestID != e.RequestID)
                return;

            if (Items.Count == 0)
                DroppedDown = true;

            var lst = new List<IContract>(e.Contracts);
            lst.Sort(new LookupContractSorter(Text));
            Items.Clear();
            lst.ForEach(c => Items.Add(c));
            SelectionStart = Text.Length;
        }
    }
}