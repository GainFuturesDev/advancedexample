﻿using System;
using System.Collections.Generic;
using System.Linq;
using GF.Api;
using GF.Api.Subscriptions;
using GF.Api.Subscriptions.Ticks;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Tick View: shows how to subscribe to tick subscriptions and handle tick responses
    /// </summary>
    public partial class TickViewControl : BaseControl
    {
        private readonly List<TickEntry> _tickEntries = new List<TickEntry>();
        private ISubscription _tickSubscription;

        /// <summary>
        ///     Default constructor
        /// </summary>
        public TickViewControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Prepares tick control events
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Subscriptions.Ticks.TicksReceived += OnTicksReceived;
            Client.Subscriptions.Bars.BarsReceived += Bars_OnBarsReceived;
        }

        private void Bars_OnBarsReceived(IGFClient client, GF.Api.Subscriptions.Bars.BarsReceivedEventArgs e)
        {
            Console.WriteLine("hmm");
        }

        private void OnTicksReceived(IGFClient client, TicksReceivedEventArgs e)
        {
            if (cbSymbol.SelectedContract == null)
                return;

            if (_tickSubscription == null || _tickSubscription.ID != e.Subscription.ID)
                return;

            if (e.Ticks.Count == 0)
                return;

            TickEntry[] tickTable = TickEntry.TickEntryConverter(e.Ticks);
            _tickEntries.AddRange(tickTable);
            if (_tickEntries.Count > 0 && _tickEntries[_tickEntries.Count - 1].TimeStamp < tickTable[0].TimeStamp)
            {
                _tickEntries.Sort((x, y) => x.TimeStamp.CompareTo(y.TimeStamp));
            }
            dgTicks.DataSource = null;
            dgTicks.DataSource = _tickEntries;
        }

        private void cbSymbol_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_tickSubscription != null)
            {
                _tickSubscription.Unsubscribe();
                _tickSubscription = null;
                _tickEntries.Clear();
            }

            dgTicks.DataSource = null;

            if (cbSymbol.SelectedContract == null)
                return;

            _tickSubscription = Client.Subscriptions.Ticks.Subscribe(cbSymbol.SelectedContract.ID,
                Client.Subscriptions.Ticks.Duration.Create(DateTime.UtcNow.AddMinutes(-10)));
        }
    }

    /// <summary>
    ///     Represent tick for visualization
    /// </summary>
    public class TickEntry
    {
        private readonly float _askPrice;
        private readonly float _bidPrice;
        private readonly float _price;
        private readonly uint _volume;
        private DateTime _timestamp;

        /// <summary>
        ///     Creates tick entry
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="price"></param>
        /// <param name="volume"></param>
        /// <param name="bidPrice"></param>
        /// <param name="askPrice"></param>
        public TickEntry(DateTime timestamp, float price, uint volume, float bidPrice, float askPrice)
        {
            _timestamp = timestamp;
            _price = price;
            _volume = volume;
            _askPrice = askPrice;
            _bidPrice = bidPrice;
        }

        /// <summary>
        ///     Tick timestamp
        /// </summary>
        public DateTime TimeStamp => _timestamp.ToLocalTime();

        /// <summary>
        ///     Tick price
        /// </summary>
        public float Price => _price;

        /// <summary>
        ///     Tick volume
        /// </summary>
        public uint Volume => _volume;

        /// <summary>
        ///     Bid price at the time of tick
        /// </summary>
        public float BidPrice => _bidPrice;

        /// <summary>
        ///     Ask price at the time of tick
        /// </summary>
        public float AskPrice => _askPrice;

        /// <summary>
        ///     Converts OEC API ticks to internal tick entries
        /// </summary>
        /// <param name="ticks"></param>
        /// <returns></returns>
        public static TickEntry[] TickEntryConverter(IReadOnlyList<Tick> ticks)
        {
            return ticks
                .Select(t => new TickEntry(t.Timestamp.ToLocalTime(), t.Price, t.Volume, t.BidPrice, t.AskPrice))
                .ToArray();
        }
    }
}