﻿using System.IO;
using System.Reflection;
using GF.Api;
using GF.Api.Accounts;
using GF.Api.Allocation;
using GF.Api.Orders.Drafts;
using GF.Api.Values.Accounts;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Global objects
    /// </summary>
    internal class Globals
    {
        /// <summary>
        ///     Allocation blocks storage file name
        /// </summary>
        public static readonly string ABSavesFileName =
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\AllocationBlocks.xml";


        private static AccountOrAB _currentAccountOrAB;

        /// <summary>
        ///     Current account or allocation block
        /// </summary>
        public static AccountOrAB CurrentAccountOrAB
        {
            get { return _currentAccountOrAB; }
            set
            {
                _currentAccountOrAB = value;
                if (OnCurrentAccountChanged != null)
                {
                    OnCurrentAccountChanged(_currentAccountOrAB);
                }
            }
        }


        public delegate void CurrentAccountChanged(AccountOrAB accountOrAB);
        /// <summary>
        ///     Occurs when current account or allocation block changed
        /// </summary>
       // public static event OnBalanceChangedEvent OnCurrentAccountChanged;
       public static event CurrentAccountChanged OnCurrentAccountChanged;

        /// <summary>
        ///     Sets current account or allocation block to order draft
        /// </summary>
        /// <param name="draft">Order draft for which to set current account or allocation block</param>
        public static void SetDraftAccount(IGFClient client, OrderDraftBuilder draft)
        {
            IAccount curAccount = null;
            var accountType = AccountType.Customer;

            foreach (var account in client.Accounts.Get())
            {
                if (account.Type != accountType) continue;
                curAccount = account;
                break;
            }

            draft.WithAccountID(curAccount?.ID);

            var currentAccount = CurrentAccountOrAB.Account;
            if (currentAccount != null)
            {
                draft.WithAccountID(curAccount?.ID);
                draft.WithAllocationBlock(null);
            }
            var currentAb = CurrentAccountOrAB.AllocationBlock;
            if (currentAb != null)
            {
                draft.WithAccountID(null);
                draft.WithAllocationBlock(AllocationBlockTemplate.Create(currentAb));
            }
        }
    }
}