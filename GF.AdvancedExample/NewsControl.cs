using GF.Api;

namespace GF.AdvancedExample
{
    /// <inheritdoc />
    /// <summary>
    ///     News control. Displays incoming news messages
    /// </summary>
    public partial class NewsControl : BaseControl
    {
        /// <summary>
        ///     News control constructor
        /// </summary>
        public NewsControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Prepares news control related events
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Connection.Aggregate.LoginCompleted += OnLoginComplete;
            Client.Messaging.Notifications.NotificationMessageReceived += OnNotificationMessage;
        }

        /// <summary>
        ///     Refresh news list when new message incomes
        /// </summary>
        private void OnNotificationMessage(IGFClient client, GF.Api.Messaging.Notifications.NotificationMessageEventArgs e)
        {
            RefreshNews();
        }

        /// <summary>
        ///     Refresh news when login complete
        /// </summary>
        private void OnLoginComplete(IGFClient client, GF.Api.Connection.LoginCompleteEventArgs e)
        {
            RefreshNews();
        }

        /// <summary>
        ///     Refreshes news list
        /// </summary>
        private void RefreshNews()
        {
            lbNews.Items.Clear();
            foreach (var message in Client.Messaging.Notifications.Get())
            {
                string time = message.Timestamp.ToShortDateString() + " " + message.Timestamp.ToShortTimeString();
                lbNews.Items.Add($"{time} {message.Channel} : {message.Message}");
            }
        }
    }
}