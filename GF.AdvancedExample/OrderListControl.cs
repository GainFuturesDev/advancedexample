﻿using System;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Orders;
using GF.Api.Orders.Commands;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Order list control. Maintains list of current orders in system
    /// </summary>
    public partial class OrderListControl : UserControl
    {
        protected readonly IGFClient Client;
        /// <summary>
        ///     Construct order list control
        /// </summary>
        public OrderListControl(IGFClient client)
        {
            Client = client;
            InitializeComponent();
        }

        /// <summary>
        ///     Current selected order
        /// </summary>
        public IOrder SelectedOrder
        {
            get => (IOrder) lvOrders.FocusedItem?.Tag;
            set
            {
                if (value == null)
                {
                    return;
                }

                foreach (object i in lvOrders.Items)
                {
                    if (!(i is ListViewItem item)) continue;
                    if (!(item.Tag is IOrder order)) continue;
                    if (order.ID == value.ID)
                    {
                        lvOrders.FocusedItem = item;
                    }
                    item.Selected = order.ID == value.ID;
                }
            }
        }

        /// <summary>
        ///     Prepares events
        /// </summary>
        protected override void OnLoad(EventArgs eventArgs)
        {
            if (Client == null) return;
            Client.Connection.Aggregate.LoginCompleted += (o,e) => UpdateOrders();
            Client.Orders.OrderConfirmed += OnOrderConfirmed;
            Client.Orders.OrderFilled += OnOrderFilled;
            Client.Orders.OrderStateChanged += OnOrderStateChanged;
            Client.Orders.CommandUpdated += OnCommandUpdated;
            if (Client.Connection.Aggregate.IsConnected)
                UpdateOrders();
        }

        /// <summary>
        ///     Updates orders control
        /// </summary>
        public void UpdateOrders()
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            lvOrders.BeginUpdate();

            lvOrders.Items.Clear();
            foreach (var order in Client.Orders.Get())
            {
                var item = new ListViewItem(order.ID.ToString()) {Tag = order};
                item.SubItems.Add(GetAccountOrAB(order));
                item.SubItems.Add(order.Side.ToString());
                item.SubItems.Add(order.Contract.ToString());
                item.SubItems.Add(order.Quantity.ToString());
                item.SubItems.Add(order.CurrentState.ToString());
                item.SubItems.Add(order.TypePriceString);
                item.SubItems.Add(order.Timestamp.ToLocalTime().ToString("HH:mm:ss.fff"));
                item.SubItems.Add(order.Comments);

                lvOrders.Items.Insert(0, item);
            }

            lvOrders.EndUpdate();

            if (lvOrders.Items.Count > 0 && SelectedOrderChanged != null)
                SelectedOrderChanged((IOrder) lvOrders.Items[0].Tag);
        }

        /// <summary>
        ///     Gets account or allocation block of orders, depends on which is specified
        /// </summary>
        /// <param name="order">Analized order</param>
        /// <returns>Returns string representation of allocation block or account</returns>
        public static string GetAccountOrAB(IOrder order)
        {
            if (order.Account != null)
                return order.Account.ToString();

            if (order.AllocationBlock != null)
                return order.AllocationBlock.ToString();

            return string.Empty;
        }

        /// <summary>
        ///     Updates Order control when command updated
        /// </summary>
        private void OnCommandUpdated(IGFClient client, CommandUpdatedEventArgs eventArgs)
        {
            UpdateOrders();
        }

        /// <summary>
        ///     Updates Order control when state of order is changed
        /// </summary>
        private void OnOrderStateChanged(IGFClient client, OrderStateChangedEventArgs eventArgs)
        {
            UpdateOrders();
        }

        /// <summary>
        ///     Updates Order control when order is filled
        /// </summary>
        private void OnOrderFilled(IGFClient client, OrderFilledEventArgs eventArgs)
        {
            UpdateOrders();
        }

        /// <summary>
        ///     Updates Order control when order is confirmed
        /// </summary>
        private void OnOrderConfirmed(IGFClient client, OrderConfirmedEventArgs eventArgs)
        {
            UpdateOrders();
        }

        /// <summary>
        ///     Occurs when selected order changes
        /// </summary>
        public event CurrentOrderChangedEventHandler SelectedOrderChanged;

        private void lvOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedOrderChanged?.Invoke(SelectedOrder);
        }
    }
}