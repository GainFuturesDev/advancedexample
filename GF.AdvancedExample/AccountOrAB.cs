﻿using System.Collections.Generic;
using System.Linq;
using GF.Api.Accounts;
using GF.Api.Allocation;
using GF.Api.Balances;
using GF.Api.Positions;

namespace GF.AdvancedExample
{

    public class AccountOrAB
    {
        public IAccount Account
        {
            get;
            set;
        }

        public IAllocationBlock AllocationBlock
        {
            get;
            set;
        }

        public AccountOrAB(IAccount account)
            : this(account, null)
        {

        }

        public AccountOrAB(IAllocationBlock allocationBlock)
            : this(null, allocationBlock)
        {
        }

        private AccountOrAB(IAccount account, IAllocationBlock block)
		{
            Account = account;
			AllocationBlock = block;
        }

        public IBalance TotalBalance => Account != null ? Account.TotalBalance : AllocationBlock.TotalBalance;

        public IEnumerable<IPosition> AvgPositions
        {
            get
            {
                if (AllocationBlock != null)
                    foreach (var pos in AllocationBlock.AvgPositions.Values)
                        yield return pos;
                else
                {
                    foreach (var pos in Account.AvgPositions.Values)
                        yield return pos;
                }
            }
        }

        public IEnumerable<IAccount> AllAccounts
        {
            get
            {
                if (Account != null)
                    yield return Account;
                else
                {
                    foreach (var blockItem in AllocationBlock.Items)
                        yield return blockItem.Value.Account;
                }
            }
        }

        public bool HasAccount(IAccount account)
            => AllAccounts.Any(a => a.ID == account.ID);

        public override string ToString()
            => Account != null ? Account.ToString() : AllocationBlock.ToString();
    }
}
