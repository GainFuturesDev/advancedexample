using System;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Users;

namespace GF.AdvancedExample
{
    /// <inheritdoc />
    /// <summary>
    ///     Chat control. Provides send/receive chat messages functionality. Displays available user list
    /// </summary>
    public partial class ChatControl : BaseControl
    {
        /// <inheritdoc />
        /// <summary>
        ///     Chat control constructor
        /// </summary>
        public ChatControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Prepares chat control related events
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Messaging.Chat.ChatMessageReceived += OnChatMessage;
            Client.Users.UserStatusChanged += OnUserStatusChanged;
            Client.Connection.Aggregate.LoginCompleted += Client_OnLoginComplete;
        }

        /// <summary>
        ///     Occurs when user status is changed, refreshes user list
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnUserStatusChanged(IGFClient client, GF.Api.Users.UserStatusChangedEventArgs e)
        {
            RefreshUsers();
        }

        /// <summary>
        ///     Refreshes events and user list when login complete
        /// </summary>
        private void Client_OnLoginComplete(IGFClient client, GF.Api.Connection.LoginCompleteEventArgs e)
        {
            RefreshUsers();
            RefreshMessages();
        }

        /// <summary>
        ///     Refreshes all messages
        /// </summary>
        private void RefreshMessages()
        {
            lbMessagesLog.Items.Clear();

            foreach (var message in Client.Messaging.Chat.Get())
            {
                // if system message, do not output timestamp
                string time;
                if (message.Timestamp.Ticks == 0)
                    time = string.Empty;
                else
                    time = message.Timestamp.ToShortDateString() + " " + message.Timestamp.ToShortTimeString();

                lbMessagesLog.Items.Add($"{time} To: {message.ToUser} From: {message.FromUser} {message.Message}");
            }
        }

        /// <summary>
        ///     refreshes list of users
        /// </summary>
        private void RefreshUsers()
        {
            var userList = Client.Users.Get().ToList();
            lbUsers.DataSource = userList;
        }

        /// <summary>
        ///     Occurs when new message added to Messages list
        /// </summary>
        private void OnChatMessage(IGFClient client, GF.Api.Messaging.Chat.ChatMessageEventArgs e)
        {
            RefreshMessages();
        }

        /// <summary>
        ///     Sends message
        /// </summary>
        private void btSend_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            if (lbUsers.SelectedItem == null)
                return;

            if (edMessage.Text.Trim() == string.Empty)
                return;

            var user = (User) lbUsers.SelectedItem;
            Client.Messaging.Chat.SendMessage(user.ID, edMessage.Text);
            RefreshMessages();
            edMessage.Text = string.Empty;
        }

        /// <summary>
        ///     formats user list output, makes (+) prefix for online users and (-) for offline
        /// </summary>
        private void lbUsers_Format(object sender, ListControlConvertEventArgs e)
        {
            var userItem = (User) e.ListItem;
            e.Value = (userItem.IsOnline ? "(+)" : "(-)") + e.Value;
        }
    }
}