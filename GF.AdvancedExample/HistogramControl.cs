using GF.Api;
using GF.Api.Contracts;
using GF.Api.Subscriptions;
using GF.Api.Subscriptions.Histograms;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Histogram control. Draws price/volume histogram
    /// </summary>
    public partial class HistogramControl : BaseControl
    {
        /// <summary>
        ///     Current histogram contract
        /// </summary>
        private IContract _currentContract;

        /// <summary>
        ///     Current subscription
        /// </summary>
        private ISubscription _currentSubscription;

        /// <summary>
        ///     Histogram control constructor
        /// </summary>
        public HistogramControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Prepares histogram event
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Subscriptions.Histogram.HistogramReceived += OnHistogramReceived;
        }

        /// <summary>
        ///     Occurs when new contract selected
        /// </summary>
        /// <param name="contract">New contract</param>
        public void ChartSelectedContractChangedEventHandler(IContract contract)
        {
            //clears last subscription
            histogramChart.ClearHistogram();
            dgHistogram.DataSource = null;

            _currentSubscription?.Unsubscribe();
            _currentSubscription = null;

            if (contract == null)
                return;

            _currentContract = contract;

            Client.Subscriptions.Histogram.Subscribe(contract.ID);
        }

        /// <summary>
        ///     Occurs when subscribed histogram received
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="histogram"></param>
        private void OnHistogramReceived(IGFClient client, HistogramReceivedEventArgs eventArgs)
        {
            // if current contract has been changed, received Histogram doesn't need to be shown
            if (_currentContract == null || eventArgs.Subscription.Contract.ID != _currentContract.ID)
                return;

            if (eventArgs.Histogram.Count == 0)
                return;

            _currentSubscription = eventArgs.Subscription;

            HistEntry[] simpleHistogram = ConvertToSimpleHistogram(eventArgs.Histogram);

            dgHistogram.DataSource = simpleHistogram;
            histogramChart.DrawHistogram(_currentSubscription.Contract.Description, simpleHistogram);
        }

        /// <summary>
        ///     Converts OEC API Histogram instance to array of HistEntry
        /// </summary>
        /// <param name="histogram"></param>
        /// <returns></returns>
        private HistEntry[] ConvertToSimpleHistogram(IHistogram histogram)
        {
            var result = new HistEntry[histogram.Count];
            for (int i = 0; i < histogram.Count; i++)
            {
                result[i] = new HistEntry(i, histogram.GetPriceAt(i), histogram.GetVolumeAt(i));
            }
            return result;
        }
    }
}