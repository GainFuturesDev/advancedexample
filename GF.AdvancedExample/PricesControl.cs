using System;
using System.Linq;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Contracts;
using GF.Api.Subscriptions.Price;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Prices control. Displays quotes, provides subscribe/unsubscribe prices for contract functionality
    /// </summary>
    public partial class PricesControl : SubscribeControl
    {


        /// <summary>
        ///     Prices control constructor
        /// </summary>
        public PricesControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
            Caption = "Quotes";
        }

        /// <summary>
        ///     Prepares event handlers for Prices control.
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();
            Client.Subscriptions.Price.PriceChanged += OnPriceChanged;
        }

        /// <summary>
        ///     Refresh quotes when price changed
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnPriceChanged(IGFClient client, PriceChangedEventArgs e)
        {
            lvQuotes.BeginUpdate();

            string keyContract = e.Contract.Symbol;

            ListViewItem item;
            if (lvQuotes.Items.ContainsKey(keyContract))
            {
                item = lvQuotes.Items[keyContract];
                while (item.SubItems.Count > 1)
                    item.SubItems.RemoveAt(1);
                AddSubItems(item, e.Contract, e.Price);
            }
            else
            {
                item = new ListViewItem {Name = keyContract, Focused = true, Tag = e.Contract};
                    //first element empty, as key only
                AddSubItems(item, e.Contract, e.Price);
                lvQuotes.Items.Add(item);
            }

            lvQuotes.EndUpdate();
            RefreshSubscribeButtonStatus();
        }

        /// <summary>
        ///     Fulfills row of quotes detailed list view
        /// </summary>
        /// <param name="item"></param>
        /// <param name="contract"></param>
        /// <param name="price"></param>
        private void AddSubItems(ListViewItem item, IContract contract, IPrice price)
        {
            item.SubItems.Add(contract.ToString());
            item.SubItems.Add(contract.PriceToString(price.LastPrice));
            item.SubItems.Add(contract.PriceToString(price.LastVol));
            item.SubItems.Add(contract.PriceToString(price.AskPrice));
            item.SubItems.Add(contract.PriceToString(price.AskVol));
            item.SubItems.Add(contract.PriceToString(price.BidPrice));
            item.SubItems.Add(contract.PriceToString(price.BidVol));
        }


        /// <summary>
        ///     Perform subscribe on selected contract
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSubscribe_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed || cbSymbol.SelectedContract == null)
                return;

            try
            {
                Client.Subscriptions.Price.Subscribe(cbSymbol.SelectedContract.ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't subscribe contract. " + ex.Message);
            }
        }

        /// <summary>
        ///     Perform unsubscribe from selected contract
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btUnsubscribe_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed || cbSymbol.SelectedContract == null)
                return;

            try
            {
                Client.Subscriptions.Get().OfType<IPriceSubscription>()
                    .FirstOrDefault(s => s.Contract.ID == cbSymbol.SelectedContract.ID)?.Unsubscribe();
                lvQuotes.Items.RemoveByKey(cbSymbol.SelectedContract.Symbol);
                RefreshSubscribeButtonStatus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't unsubscribe contract. " + ex.Message);
            }
        }

        /// <summary>
        ///     Occurs when selected contract changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbSymbol_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSymbol.SelectedContract == null)
                return;
            RefreshSubscribeButtonStatus();
        }

        /// <summary>
        ///     Refreshes Subscribe and Unsubscribe buttons statuses. If list of subscribed contracts contains selected contract,
        ///     then make Unsubscribe button available
        /// </summary>
        private void RefreshSubscribeButtonStatus()
        {
            btSubscribe.Enabled = cbSymbol.SelectedContract != null &&
                                  !lvQuotes.Items.ContainsKey(cbSymbol.SelectedContract.Symbol);
            btUnsubscribe.Enabled = !btSubscribe.Enabled;
        }

        /// <summary>
        ///     Matches the selected contract in the combo, when a user clicks on the DataGrid with quotes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvQuotes_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            cbSymbol.SelectedItem = e.Item.Tag;
        }
    }
}