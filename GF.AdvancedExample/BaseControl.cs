﻿using System;
using System.Windows.Forms;
using GF.Api;

namespace GF.AdvancedExample
{
    /// <inheritdoc />
    /// <summary>
    ///     Base class for all controls in example
    /// </summary>
    public partial class BaseControl : UserControl
    {
        protected readonly IGFClient Client;
        /// <inheritdoc />
        /// <summary>
        ///     Base control constructor
        /// </summary>
        public BaseControl(IGFClient client)
        {
            Client = client;
            InitializeComponent();
        }

        /// <summary>
        ///     Control's caption
        /// </summary>
        public string Caption
        {
            get => lbCaption.Text;
            set => lbCaption.Text = value;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Prepares handlers
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!DesignMode)
                PrepareControlEventHandlers();
        }

        /// <summary>
        ///     Prepares derived control's event handlers
        /// </summary>
        protected virtual void PrepareControlEventHandlers()
        {
            //do nothing
        }
    }
}