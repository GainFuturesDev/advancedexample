using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Orders;
using GF.Api.Orders.Drafts;
using GF.Api.Values.Orders;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Orders control. Provides submitting, modifying, canceling orders. Also OCO and Linked orders are supported
    /// </summary>
    public partial class OrdersControl : OrderDraftControl
    {
        /// <summary>
        ///     Orders control constructor
        /// </summary>
        public OrdersControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
            Caption = "Orders";
        }

        /// <summary>
        ///     Submit contract sending contract to server
        /// </summary>
        private void btSubmit_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;
            try
            {
                var builder = new OrderDraftBuilder();
                if (!FillOutOrderDraft(builder, "Regular"))
                    return;

                var draft = builder.Build();

                if (MessageBox.Show("Send order " + Client.Strings.DraftToString(draft), "Confirmation", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) ==
                    DialogResult.No)
                    return;
                Client.Orders.SendOrder(draft);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error sending order : " + ex.Message);
            }
        }

        /// <summary>
        ///     Clears controls, preparing new order, when New button clicked
        /// </summary>
        private void btNew_Click(object sender, EventArgs e)
        {
            ClearOrderDraft();
        }

        /// <summary>
        ///     Cancels selected order
        /// </summary>
        private void btCancel_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            if (orderList.SelectedOrder == null)
                return;

            var order = orderList.SelectedOrder;

            if (
                MessageBox.Show("Cancel order #" + order.ID, "Confirmation", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) ==
                DialogResult.No)
                return;

            Client.Orders.CancelOrder(order.ID);
        }

        /// <summary>
        ///     Modifying selected order
        /// </summary>
        private void btModify_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            if (orderList.SelectedOrder == null)
                return;

            var selectedOrder = orderList.SelectedOrder;
            var selectedDraft = new ModifyOrderDraftBuilder().FromOrder(selectedOrder);
            if (!FillOutOrderDraft(selectedDraft))
                return;

            if (
                MessageBox.Show($"Modify order \'{selectedOrder}\' with \'{selectedDraft}\'", "Confirmation",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                DialogResult.No)
                return;

            Client.Orders.ModifyOrder(selectedDraft.Build());
        }

        /// <summary>
        ///     Occurs when current order changed
        /// </summary>
        public event CurrentOrderChangedEventHandler OnCurrentOrderChanged;

        /// <summary>
        ///     Occurs when current selected order is changed
        /// </summary>
        private void orderList_SelectedOrderChanged(IOrder currentOrder)
        {
            pnlOrderLinks.UpdateOrder(currentOrder);
            if (currentOrder != null)
            {
                SetDraftFromOrder(currentOrder);
                OnCurrentOrderChanged?.Invoke(currentOrder);
            }
        }

        /// <summary>
        ///     Sends OCO orders
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSendOCO_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            //Creating fist draft
            var orderDraftBuilder1 = new OrderDraftBuilder();
            if (!FillOutOrderDraft(orderDraftBuilder1, "OCO"))
                return;

            //First draft is of LMT
            if (!double.TryParse(edLimitLegPrice.Text, out var priceLimitLegPrice))
                return;
            orderDraftBuilder1.WithOrderType(OrderType.Limit);
            orderDraftBuilder1.WithPrice(priceLimitLegPrice);
            var draft1 = orderDraftBuilder1.Build();

            //Creating second draft
            var orderDraftBuilder2 = new OrderDraftBuilder()
                .WithQuantity(draft1.Quantity)
                .WithContractID(draft1.ContractID)
                .WithSide(draft1.Side)
                .WithComments("OCO");

            Globals.SetDraftAccount(Client, orderDraftBuilder2);

            //Second draft is of STP
            if (!double.TryParse(edStopLegPrice.Text, out var priceStopLegPrice))
                return;
            orderDraftBuilder2.WithOrderType(OrderType.Stop);
            orderDraftBuilder2.WithPrice(priceStopLegPrice);

            var draft2 = orderDraftBuilder2.Build();

            if (
                MessageBox.Show("Send OCO orders \n" + draft1 + "\n" + draft2, "Confirmation", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) ==
                DialogResult.No)
                return;

            try
            {

                Client.Orders.SendOCOOrders(draft1, draft2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error sending order : " + ex.Message);
            }
        }

        /// <summary>
        ///     Sends Linked orders
        /// </summary>
        private void btSendBracket_Click(object sender, EventArgs e)
        {
            if (Client.Connection.Aggregate.IsClosed)
                return;

            //Creating fist draft
            var mainOrderDraftBuilder = new OrderDraftBuilder();
            if (!FillOutOrderDraft(mainOrderDraftBuilder, "OSO"))
                return;

            var mainOrder = mainOrderDraftBuilder.Build();

            var draft1 = new OrderDraftBuilder()
                    .WithQuantity(mainOrder.Quantity)
                    .WithContractID(mainOrder.ContractID);

            Globals.SetDraftAccount(Client, draft1);

            var draft2 = new OrderDraftBuilder()
                .WithQuantity(mainOrder.Quantity)
                .WithContractID(mainOrder.ContractID);

            Globals.SetDraftAccount(Client, draft2);

            var isLimitOrderFulfilled = FulfillLimitDraft(mainOrder, draft1);
            var isStopOrderFulfilled = FulfillStopDraft(mainOrder, draft2);

            if (!isLimitOrderFulfilled && !isStopOrderFulfilled)
                return;

            try
            {
                if (isLimitOrderFulfilled && isStopOrderFulfilled)
                {
                    if (
                        MessageBox.Show("Send linked orders \n" + mainOrder + "\n" + draft1 + "\n" + draft2,
                            "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.No)
                        return;

                    Client.Orders.SendOSOOrders(mainOrder, draft1.Build(), draft2.Build(), OSOGroupingMethod.ByFirstPrice);
                }
                else
                {
                    var oneDraft = isLimitOrderFulfilled ? draft1 : draft2;

                    if (
                        MessageBox.Show("Send linked orders \n" + mainOrder + "\n" + oneDraft, "Confirmation",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.No)
                        return;

                    Client.Orders.SendOSOOrders(mainOrder, oneDraft.Build(), null, OSOGroupingMethod.ByFirstPrice);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error sending orders : " + ex.Message);
            }
        }

        /// <summary>
        ///     Fullfills limit order for Bracket
        /// </summary>
        /// <param name="mainOrder">Main order of bracket</param>
        /// <param name="draft">order draft to fullfill</param>
        /// <returns>true if order fullfilled, otherwise false</returns>
        private bool FulfillLimitDraft(OrderDraft mainOrder, OrderDraftBuilder draft)
        {
            draft.WithComments("OSO");
            draft.WithOrderType(OrderType.Limit);
            if (!double.TryParse(edLimitFactor.Text, out var limitPriceFactor))
            {
                return false;
            }

            var side = GetOppositeSide(mainOrder);
            draft.WithSide(side);

            if (side == OrderSide.Buy)
                draft.WithPrice(mainOrder.Price - limitPriceFactor);
            else
                draft.WithPrice(mainOrder.Price + limitPriceFactor);

            return true;
        }

        /// <summary>
        ///     Fulfills stop order for Bracket
        /// </summary>
        /// <param name="mainOrder">Main order of bracket</param>
        /// <param name="draft">Order draft to fulfill</param>
        /// <returns>True if order fulfilled, otherwise false</returns>
        private bool FulfillStopDraft(OrderDraft mainOrder, OrderDraftBuilder draft)
        {
            draft.WithComments("OSO");
            draft.WithOrderType(OrderType.Stop);
            if (!double.TryParse(edStopFactor.Text, out var stopPriceFactor))
            {
                return false;
            }

            var side = GetOppositeSide(mainOrder);
            draft.WithSide(side);

            if (side == OrderSide.Buy)
                draft.WithPrice(mainOrder.Price + stopPriceFactor);
            else
                draft.WithPrice(mainOrder.Price - stopPriceFactor);

            return true;
        }

        /// <summary>
        ///     Gets opposite side from order
        /// </summary>
        /// <param name="mainOrder">Order</param>
        /// <returns>Opposite side of order</returns>
        private OrderSide GetOppositeSide(OrderDraft mainOrder)
            => mainOrder.Side == OrderSide.Sell ? OrderSide.Buy : OrderSide.Sell;

        /// <summary>
        ///     Handles order navigation event and updates order list to the navigated order.
        /// </summary>
        /// <param name="target">Target order</param>
        private void pnlOrderLinks_OrderLinkNavigated(IOrder target)
        {
            orderList.SelectedOrder = target;
        }
    }

    /// <summary>
    ///     Represents event handler
    /// </summary>
    /// <param name="currentOrder">New order</param>
    public delegate void CurrentOrderChangedEventHandler(IOrder currentOrder);
}