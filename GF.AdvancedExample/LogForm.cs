﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using GF.Api;
using GF.Api.Logging;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     OEC API logs output window
    /// </summary>
    public partial class LogForm : Form
    {
        private static LogForm _logForm;
        private readonly IGFClient _gfClient;
        private const int MaxLogItems = 7000;

        /// <summary>
        ///     Creates OEC API logs output window
        /// </summary>
        public LogForm(IGFClient client)
        {
            _gfClient = client;
            InitializeComponent();
            //Lets name main thread
            if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                Thread.CurrentThread.Name = "Main";
        }

        /// <summary>
        ///     Set up custom logger on load to ensure that logBox control is ready for output
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            _gfClient.Logging.NewMessageLogged += OnNewLogMessage;
            UpdateLoggingConfig();
        }

        private void OnNewLogMessage(IGFClient client, NewLogMessageEventArgs e)
        {
            try
            {
                string thread = Thread.CurrentThread.Name;
                if (String.IsNullOrEmpty(thread))
                    thread = "System #" + Thread.CurrentThread.GetHashCode();
                DateTime time = DateTime.Now;

                // caller can be from diffrent thread
                logBox.BeginInvoke(new MethodInvoker(() =>
                {
                    try
                    {
                        if (logBox.Items.Count > MaxLogItems)
                            logBox.Items.Clear();

                        logBox.Items.Add($"{time}: ({thread}) {e.Category}: {e.Message}");
                    }
                    catch
                    {
                        //should be handled here
                    }
                }));
            }
            catch
            {
                //should be handled here
            }
        }

        /// <summary>
        ///     Shows log window
        /// </summary>
        public static void ShowLog(IGFClient client)
        {
            if (_logForm == null) _logForm = new LogForm(client);
            _logForm.Show();
        }

        /// <summary>
        ///     To prevent form disposing
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
            base.OnClosing(e);
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            logBox.Items.Clear();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void chbTransport_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLoggingConfig();
        }

        private void UpdateLoggingConfig()
        {
            var level = LogCategory.Core;
            if (chbApplication.Checked)
                level |= LogCategory.Application;
            if (chbPriceFeed.Checked)
                level |= LogCategory.PriceFeed;
            if (chbTransport.Checked)
                level |= LogCategory.Transport;
            _gfClient.Logging.SetCategories(level);
        }

        private void edIndexNum_ValueChanged(object sender, EventArgs e)
        {
            UpdateLoggingConfig();
        }
    }
}