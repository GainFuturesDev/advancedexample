using System;
using GF.Api;

namespace GF.AdvancedExample
{
    /// <summary>
    ///     Event log control. Displays order related events to log window
    /// </summary>
    public partial class EventLogControl : BaseControl
    {
        /// <summary>
        ///     Event log control constructor
        /// </summary>
        public EventLogControl(IGFClient client)
            : base(client)
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Prepares events which must be logged
        /// </summary>
        protected override void PrepareControlEventHandlers()
        {
            base.PrepareControlEventHandlers();

            Client.Orders.OrderConfirmed += OnOrderConfirmed;
            Client.Orders.OrderFilled += OnOrderFilled;
            Client.Orders.OrderStateChanged += OnOrderStateChanged;
            Client.Orders.CommandUpdated += OnCommandUpdated;
        }

        /// <summary>
        ///     Writes to log when command updated
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnCommandUpdated(IGFClient client, GF.Api.Orders.Commands.CommandUpdatedEventArgs e)
        {
            PrintToEventLog(DateTime.UtcNow, $"Command {e.Command} for Order #{e.Order.ID} ({e.Order}) updated");
        }

        /// <summary>
        ///     Writes to log when order state changed
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnOrderStateChanged(IGFClient client, GF.Api.Orders.OrderStateChangedEventArgs e)
        {
            PrintToEventLog(DateTime.UtcNow,
                $"State of Order #{e.Order.ID} ({e.Order}) changed from {e.PreviousOrderState} to {e.Order.CurrentState}");
        }

        /// <summary>
        ///     Writes to log when order filled
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnOrderFilled(IGFClient client, GF.Api.Orders.OrderFilledEventArgs e)
        {
            PrintToEventLog(DateTime.UtcNow, $"Order #{e.Order.ID} ({e.Order}) filled : {e.Fill}");
        }

        /// <summary>
        ///     Writes to log when order confirmed
        /// </summary>
        /// <param name="client"></param>
        /// <param name="e"></param>
        private void OnOrderConfirmed(IGFClient client, GF.Api.Orders.OrderConfirmedEventArgs e)
        {
            PrintToEventLog(DateTime.UtcNow,
                $"Order #{e.OriginalOrderID} ({e.Order}) confirmed. New Order #{e.Order.ID}");
        }

        /// <summary>
        ///     Writes custom message to log
        /// </summary>
        /// <param name="time">Time when event occured</param>
        /// <param name="message">Event message text</param>
        private void PrintToEventLog(DateTime time, string message)
        {
            edLog.Text = $@"{time.ToLocalTime():HH:mm:ss.fff} : {message}{edLog.Text}";
        }
    }
}